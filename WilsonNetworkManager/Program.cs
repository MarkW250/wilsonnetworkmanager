﻿using System;


using System.Net;
using System.Net.Sockets;
using WilsonNetworkManager;

namespace WilsonNetworkManager
{
    class Program
    {
        static void Main(string[] args)
        {
            string testIp = "88.152.184.20";

            Console.WriteLine("Hello World!");
            Console.WriteLine("");
            Console.WriteLine("");

            TestGetIps();


            Console.WriteLine("Initialise Network Manager...");
            WilsonNetworkManager myWNM = new WilsonNetworkManager();

            Console.WriteLine("Initialised");
            Console.WriteLine("My External IP Address: " + myWNM.ExternalIpAddress);
            Console.WriteLine("My Local IP Address: " + myWNM.LocalIpAddress);

            while (true)
            {
                string input = Console.ReadLine();

                if (input.Contains("quit"))
                {
                    break;
                }
                else if(input.Contains("rec"))
                {
                    myWNM.ReceivePacket();
                }
                else if (input.Contains("send"))
                {
                    myWNM.SendPacket(testIp);
                }
                else if(input.Contains("ip:"))
                {
                    testIp = input.Split(':')[1].Trim();
                    Console.WriteLine("New IP to send to: " + testIp);
                }
            }



            //TestGetIps();
            //TestGetExternalIp();

            Console.WriteLine("");
            Console.WriteLine("************** END *********************");
            Console.WriteLine("");

        }


        static void TestGetIps()
        {
            Console.WriteLine("Here are the IP's:");
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                //if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    Console.WriteLine("IP: " + ip.ToString() + "   IP Type: " + ip.AddressFamily.ToString());
                }
            }
        }

        // from https://stackoverflow.com/questions/3253701/get-public-external-ip-address
        static void TestGetExternalIp()
        {
            string externalip = new WebClient().DownloadString("http://checkip.dyndns.org");
            //HTTPGet req = new HTTPGet();
            //req.Request("http://checkip.dyndns.org");
            string[] a = externalip.Split(':');
            string a2 = a[1].Substring(1);
            string[] a3 = a2.Split('<');
            string a4 = a3[0];
            Console.WriteLine(a4);
        }
    }
}
