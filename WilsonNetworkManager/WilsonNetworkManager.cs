﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;

namespace WilsonNetworkManager
{
    class WilsonNetworkManager
    {
        /* Static part */
        private static string _version = "V0.01";

        public static string VERSION
        {
            get { return _version; }
        }



        /* Instance Part */

        /* Private variables */
        bool _inited = false;

        /* Properties */
        string _myExtIp = "";
        string _myLocalIp = "";

        public string ExternalIpAddress
        {
            get
            {
                if(!_inited)
                {
                    this.Initialise();
                }
                return _myExtIp;
            }
        }

        public string LocalIpAddress
        {
            get
            {
                if (!_inited)
                {
                    this.Initialise();
                }
                return _myLocalIp;
            }
        }

        /* Public Methods */
        public WilsonNetworkManager(bool initilialise = true)
        {
            if(initilialise)
            {
                this.Initialise();
            }
        }

        public void Initialise()
        {
            _myExtIp = getExternalIp();
            _myLocalIp = getLocalIPAddress();
            _inited = true;
        }

        public void SendPacket(string ip)
        {
            Socket sock = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

            IPAddress serverAddr = IPAddress.Parse(ip);

            IPEndPoint endPoint = new IPEndPoint(serverAddr, 11000);

            Console.Write("Sending test packet to: " + ip);

            string text = "Hello";
            byte[] send_buffer = Encoding.ASCII.GetBytes(text);

            sock.SendTo(send_buffer, endPoint);
        }

        public void ReceivePacket() 
        {
            IPEndPoint ServerEndPoint = new IPEndPoint(IPAddress.Any, 11000);
            Socket WinSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            WinSocket.Bind(ServerEndPoint);

            Console.Write("Waiting for client");
            IPEndPoint sender = new IPEndPoint(IPAddress.Any, 0);
            EndPoint Remote = (EndPoint)(sender);
            byte[] data = new byte[256];
            int recv = WinSocket.ReceiveFrom(data, ref Remote);
            Console.WriteLine("Message received from {0}:", Remote.ToString());
            Console.WriteLine(Encoding.ASCII.GetString(data, 0, recv));
        }

        /* Private Methods */

        /*
        * GetLocalIPAddress
        * Get's IP address of machine on local network
        */
        private string getLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("No network adapters with an IPv4 address in the system!");
        }

        /*
         * GetExternalIp
         * Get IP address of machine as seen by interwebz
         */
        // from https://stackoverflow.com/questions/3253701/get-public-external-ip-address
        private string getExternalIp()
        {
            string externalip = new WebClient().DownloadString("http://checkip.dyndns.org");    // Current IP Address: 88.152.184.20 
            string[] a = externalip.Split(':');                                                 // [Current IP Address] [ 88.152.184.20] 
            string a2 = a[1].Substring(1);                                                      // 88.152.184.20
            string[] a3 = a2.Split('<');                                                        // [88.152.184.20]
            string a4 = a3[0];                                                                  // 88.152.184.20
            return a4;
        }


    }
}
